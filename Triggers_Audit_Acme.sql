/******************************************************************************
**				   	        CREATION OF TRIGGERS		   					 **
*******************************************************************************/
/******************************************************************************
**  Author: Marvin Mendia													 **
**  Date: 06/19/2018														 **
*******************************************************************************
**                            Change History								 **
*******************************************************************************
**   Date:				 Author:                         Description:		 **
** --------			-------------        ----------------------------------- **
** 06/19/2018		Marvin Mendia			Initial version					 **
** 06/22/2018       Miguel Lopez            Adding Triggers for Employee     ** 
*******************************************************************************/
USE Acme
GO
PRINT 'Start of Script Execution....';
GO

/******************************************************************************
**				 Name: TG_Training(Audit)_InsertUpdate					 **
**				 Desc: Audit History for Training table						 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[TG_Training(Audit)_InsertUpdate]') 
		AND type in (N'TR'))
BEGIN
	DROP TRIGGER [dbo].[TG_Training(Audit)_InsertUpdate]
END
GO
CREATE TRIGGER [dbo].[TG_Training(Audit)_InsertUpdate]
ON [dbo].[Training]
FOR INSERT, UPDATE
AS
BEGIN
  IF TRIGGER_NESTLEVEL(@@ProcID) > 1 
    RETURN
 
  SET NOCOUNT ON;
  SET XACT_ABORT ON;
 
  DECLARE @CurrDate DATETIME = GETUTCDATE();
 
  IF UPDATE(name)
  BEGIN
    INSERT INTO dbo.AuditHistory(TableName, 
                                 ColumnName, 
                                 ID, 
                                 Date, 
                                 OldValue, 
                                 NewValue,
								 ModifiedBy) 
    SELECT TableName    = 'Training', 
           ColumnName   = 'name',
           ID1          = i.id, 
           Date         = @CurrDate, 
           OldValue     = d.[name], 
           NewValue     = i.[name],
           ModifiedBy   = i.ModifiedBy          
    FROM deleted d 
    FULL OUTER JOIN inserted i ON (d.id = i.id)
    WHERE ISNULL(d.name, '') != ISNULL(i.name, '');
  END
END
GO
PRINT 'Trigger [TG_Training(Audit)_InsertUpdate] created';
GO

/******************************************************************************
**				 Name: TG_Position(Audit)_InsertUpdate						 **
**				 Desc: Audit History for Position table						 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[TG_Position(Audit)_InsertUpdate]') 
		AND type in (N'TR'))
BEGIN
	DROP TRIGGER [dbo].[TG_Position(Audit)_InsertUpdate]
END
GO
CREATE TRIGGER [dbo].[TG_Position(Audit)_InsertUpdate]
ON [dbo].[Position]
FOR INSERT, UPDATE
AS
BEGIN
  IF TRIGGER_NESTLEVEL(@@ProcID) > 1 
    RETURN
 
  SET NOCOUNT ON;
  SET XACT_ABORT ON;
 
  DECLARE @CurrDate DATETIME = GETUTCDATE();
 
  IF UPDATE(name)
  BEGIN
    INSERT INTO dbo.AuditHistory(TableName, 
                                 ColumnName, 
                                 ID, 
                                 Date, 
                                 OldValue, 
                                 NewValue,
								 ModifiedBy) 
    SELECT TableName    = 'Position', 
           ColumnName   = 'name',
           ID1          = i.id, 
           Date         = @CurrDate, 
           OldValue     = d.[name], 
           NewValue     = i.[name],
           ModifiedBy   = i.ModifiedBy          
    FROM deleted d 
    FULL OUTER JOIN inserted i ON (d.id = i.id)
    WHERE ISNULL(d.name, '') != ISNULL(i.name, '');
  END
END
GO
PRINT 'Trigger [TG_Position(Audit)_InsertUpdate] created';
GO
/******************************************************************************
**				 Name: TG_EmployeeTraining(Audit)_InsertUpdate				 **
**				 Desc: Audit History for Employee_Training table			 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[TG_EmployeeTraining(Audit)_InsertUpdate]') 
		AND type in (N'TR'))
BEGIN
	DROP TRIGGER [dbo].[TG_EmployeeTraining(Audit)_InsertUpdate]
END
GO
CREATE TRIGGER [dbo].[TG_EmployeeTraining(Audit)_InsertUpdate]
ON [dbo].[Employee_Training]
FOR INSERT, UPDATE
AS
BEGIN
  IF TRIGGER_NESTLEVEL(@@ProcID) > 1 
    RETURN
 
  SET NOCOUNT ON;
  SET XACT_ABORT ON;
 
  DECLARE @CurrDate DATETIME = GETUTCDATE();
 
  IF UPDATE(employee_id) OR UPDATE(training_id)
  BEGIN
    INSERT INTO dbo.AuditHistory(TableName, 
                                 ColumnName, 
                                 ID, 
                                 Date, 
                                 OldValue, 
                                 NewValue,
								 ModifiedBy) 
    SELECT TableName    = 'Employee_Training', 
           ColumnName   = 'employee_id',
           ID1          = i.id, 
           Date         = @CurrDate, 
           OldValue     = d.[employee_id], 
           NewValue     = i.[employee_id],
           ModifiedBy   = i.ModifiedBy          
    FROM deleted d 
    FULL OUTER JOIN inserted i ON (d.id = i.id)
    WHERE ISNULL(d.employee_id, '') != ISNULL(i.employee_id, '');
  END
END
GO
PRINT 'Trigger [TG_EmployeeTraining(Audit)_InsertUpdate] created';
GO


GO
/******************************************************************************
**  Name: TG_Employee(Audit)_Update
**  Desc: Audit History for Empleado table
*******************************************************************************
**                            Change History
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[TG_EmplooyeeFirstName(Audit)_Update]') 
		AND type in (N'TR'))
BEGIN
	DROP TRIGGER [dbo].[TG_EmplooyeeFirstName(Audit)_Update]
END
GO

CREATE TRIGGER [dbo].[TG_EmplooyeeFirstName(Audit)_Update]
ON [dbo].[Employee]
FOR UPDATE
AS
BEGIN
  IF TRIGGER_NESTLEVEL(@@ProcID) > 1 
    RETURN
 
  SET NOCOUNT ON;
  SET XACT_ABORT ON;
 
  DECLARE @CurrDate DATETIME = GETUTCDATE();
 
  IF UPDATE(First_Name)
  BEGIN
    INSERT INTO [dbo].[AuditHistory](TableName, 
									 ColumnName, 
									 Id, 
									 Date, 
									 OldValue, 
									 NewValue,
									 ModifiedBy) 
    SELECT TableName    = 'Employee', 
           ColumnName   = 'First_Name',
           ID1          = i.Id, 
           Date         = @CurrDate, 
           OldValue     = d.[First_Name], 
           NewValue     = i.[First_Name],
           ModifiedBy   = i.ModifiedBy          
    FROM deleted d 
    FULL OUTER JOIN inserted i ON (d.Id = i.Id)
    WHERE ISNULL(d.First_Name, '') != ISNULL(i.First_Name, '');
  END
END
  GO
PRINT 'Trigger [TG_EmplooyeeFirstName(Audit)_Update] created';
GO

/******************************************************************************
**  Name: [TG_EmployeeDni(Audit)_Update]
**  Desc: Audit History for Empleado table
*******************************************************************************
**                            Change History
*******************************************************************************/

IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'TG_EmployeeDni(Audit)_Update') 
		AND type in (N'TR'))
BEGIN
	DROP TRIGGER [dbo].[TG_EmployeeDni(Audit)_Update]
END
GO
CREATE TRIGGER [dbo].[TG_EmployeeDni(Audit)_Update]
ON [dbo].[Employee]
FOR UPDATE
AS
BEGIN
  IF TRIGGER_NESTLEVEL(@@ProcID) > 1 
    RETURN
 
  SET NOCOUNT ON;
  SET XACT_ABORT ON;
 
  DECLARE @CurrDate DATETIME = GETUTCDATE();
 
  IF UPDATE(Dni)
  BEGIN
    INSERT INTO dbo.AuditHistory(TableName, 
                                 ColumnName, 
                                 ID, 
                                 Date, 
                                 OldValue, 
                                 NewValue,
								 ModifiedBy) 
    SELECT TableName    = 'Employee', 
           ColumnName   = 'Dni',
           ID1          = i.Id, 
           Date         = @CurrDate, 
           OldValue     = d.[Dni], 
           NewValue     = i.[Dni],
           ModifiedBy   = i.ModifiedBy          
    FROM deleted d 
    FULL OUTER JOIN inserted i ON (d.Id = i.Id)
    WHERE ISNULL(d.Dni, '') != ISNULL(i.Dni, '');
  END
END
GO
PRINT 'Trigger [TG_EmployeeDni(Audit)_Update] created';
GO

/******************************************************************************
**  Name: [TG_EmployeeAddress(Audit)_Update]
**  Desc: Audit History for Empleado table
*******************************************************************************
**                            Change History
*******************************************************************************/

IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'TG_EmployeeAddress(Audit)_Update') 
		AND type in (N'TR'))
BEGIN
	DROP TRIGGER [dbo].[TG_EmployeeAddress(Audit)_Update]
END
GO
CREATE TRIGGER [dbo].[TG_EmployeeAddress(Audit)_Update]
ON [dbo].[Employee]
FOR UPDATE
AS
BEGIN
  IF TRIGGER_NESTLEVEL(@@ProcID) > 1 
    RETURN
 
  SET NOCOUNT ON;
  SET XACT_ABORT ON;
 
  DECLARE @CurrDate DATETIME = GETUTCDATE();
 
  IF UPDATE(Address)
  BEGIN
    INSERT INTO dbo.AuditHistory(TableName, 
                                 ColumnName, 
                                 ID, 
                                 Date, 
                                 OldValue, 
                                 NewValue,
								 ModifiedBy) 
    SELECT TableName    = 'Employee', 
           ColumnName   = 'Address',
           ID1          = i.Id, 
           Date         = @CurrDate, 
           OldValue     = d.[Address], 
           NewValue     = i.[Address],
           ModifiedBy   = i.ModifiedBy          
    FROM deleted d 
    FULL OUTER JOIN inserted i ON (d.Id = i.Id)
    WHERE ISNULL(d.[Address], '') != ISNULL(i.[Address], '');
  END
END
GO
PRINT 'Trigger [TG_EmployeeAddress(Audit)_Update] created';
GO

PRINT 'End of Script Execution....';
GO